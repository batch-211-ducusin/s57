let collection = [];
// Write the queue functions below. 


// 1. Gives all the elements of the queue
function print(){
    return collection;
}

// 2. Adds an element/elements to the end of the queue
function enqueue(item){
    collection[collection.length] = item;
    return collection;
}

// 3. Removes an element in front of the queue
function dequeue(){
    /*return collection.shift();*/
    for(let i=1; i<collection.length; i++) {
        collection[i-1] = collection[i]
    }
    collection.length--;
    return collection;
}

// 4. Shows the element at the front
function front(){
    return collection[0]
}

// 5. Shows the total number of elements
function size(){
    /*return collection.length*/
    let number = 0;
    for(let i of collection) {
        number++
    }
    return number;
}

// 6. Gives a Boolean value describing whether the queue is empty or not
function isEmpty(){
    /*if(collection.length === 0) {
        return true;
    } else {
        return false;
    }*/

    let number = 0;
    for(let i of collection) {
        number++
    }
    if(number === 0) {
        return true;
    } else {
        return false;
    }
}


// Export create queue functions below.
module.exports = {
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};